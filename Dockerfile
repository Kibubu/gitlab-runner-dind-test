FROM mongo:latest

WORKDIR /usr/src/app

COPY .gitlab-ci.yml .
CMD ["cat .gitlab-ci.yml"]
